---
title: "Kontak - Halimboi"
seotitle: "Kontak - Halimboi"
date: 2020-02-29T20:28:25+07:00
draft: false
type: "contact"
resources:
- src: "*.jpeg"
- src: "1.jpeg"
  name: "cover"
---

# Kontak

Jika ada yang ingin Anda sampaikan, hubungi saya melalui alamat dibawah ini :

- Telegram: [@dsnvhlm](https://t.me/dsnvhlm)
- Twitter:	[@halimboi](https://twitter.com/halimboi)

Atau gunakan form di bawah untuk mengirim pesan cepat.

***
