---
draft: true
date: 2020-02-16T14:12:24Z
title: "Quick and Easy Setup Raspberry Pi Using GNU/Linux"
subtitle: "Menyiapkan Raspberry Pi untuk membangun proyek pertama Anda"
seotitle: "Quick And Easy Setup Raspberry Pi Using GNU/Linux"
description : "Menyiapkan Raspberry Pi agar siap digunakan untuk membangun proyek pertama Anda."
slug: ""
categories:
- raspberrypi
- linux
resources:
- src: "cover.jpeg"
  name: "cover"
- src: "*.jpeg"
---


Sampurasun. Panduan singkat ini akan menjelaskan cara menyiapkan perangkat Raspberry Pi pertama
kali. Panduan ini terkait instalasi sistem operasi Raspbian GNU/Linux dan konfigurasi dasar untuk remote akses melalui SSH.
 
Sebelum memulai, berikut beberapa hal yang perlu Anda siapkan :
- Komputer dengan sistem operasi **GNU/Linux**,
- Perangkat **Raspberry Pi**,
- Sdcard dengan kapasitas >= **8GB** dan
- Koneksi internet.

***

# 1. Menginstal Sistem Operasi
**Raspbian** GNU/Linux tersedia dalam dua varian, ***desktop image*** membawa **LXDE** sebagai
lingkungan desktop dan beberapa aplikasi tambahan. ***Lite image*** merupakan varian minimal, tidak
terdapat lingkungan desktop dan hanya membawa aplikasi dasar saja, terlihat dari perbedaan ukuran
image kedua varian tersebut.

Pilih varian yang sesuai dengan kebutuhan Anda. Jika Anda menggunakan Raspberry Pi sebagai dekstop sehari-hari,
unduh ***desktop image***, namu jika berencana untuk menggunakannya sebagai server atau tidak memerlukan
GUI, pilib **lite image***.

Silahkan unduh Raspbian GNU/Linux di laman resmi Raspberry Pi :
[www.raspberrypi.org/downloads/raspbian](https://www.raspberrypi.org/downloads/raspbian/)

## Menyalin Image Kedalam Sdcard

{{< photo src=".jpeg" alt="" >}}

{{< photo class=fullwidth src=".jpeg" alt="" >}}

{{< photoset max="2" >}}
  {{< photo src=".jpeg" alt="" >}}
  {{< photo src=".jpeg" alt="" >}}
{{</ photoset >}}

{{< photoset max="3" >}}
  {{< photo src=".jpeg" alt="" >}}
  {{< photo src=".jpeg" alt="" >}}
  {{< photo src=".jpeg" alt="" >}}
{{</ photoset >}}


***
