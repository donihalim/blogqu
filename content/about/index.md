---
title: "About - Halimboi"
seotitle: "About - Halimboi"
date: 2019-07-02T23:11:56+07:00
draft: false
type: "about"
resources:
- src: "*.jpeg"
- src: "1.jpeg"
  name: "cover"
---

![Halimboi](banner.png)

# Sampurasun

Hi, saya [DØNIX](https://halimbois.netlify.com), seorang pengguna GNU/Linux biasa dari Bandung - Indonesia yang baru-baru ini sangat tertarik dengan hal-hal terkait GNU/Linux dan Open Source.

**Halimboi** sendiri merupakan blog yang membahas seputar sistem operasi GNU/Linux dan perangkat Raspberry Pi. Blog ini ditujukan sebagai tempat untuk mendokumentasikan hal-hal yang pernah saya pelajari terkait dua topik di atas.

***

Salam, **Doni Halim**.
