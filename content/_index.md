---
title: "Halimboi"
seotitle: "Halimboi - Tentang GNU/Linux dan Raspberry Pi"
description: "Blog yang membahas tentang GNU/Linux dan perangkat Raspberry Pi."
date: 2019-07-26T22:57:50+02:00
draft: false
tags:
  - linux
  - raspi
  - raspberrypi
  - floss
  - foss
  - desktop
  - terminal
  - server
  - pemula
  - lisensi
  - cc
resources:
- src: "*.jpg"
---
